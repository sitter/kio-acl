// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2020-2021 Harald Sitter <sitter@kde.org>

#include <QDebug>
#include <QCoreApplication>

#include <KIO/StatJob>
#include <KFileItem>

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    const QUrl url = QUrl::fromUserInput(app.arguments().at(1));
    auto job = KIO::stat(url);
    job->exec();
    KIO::UDSEntry entry = job->statResult();
    KFileItem item(entry, url);

    qDebug() << item;
    qDebug() << "hasExtendedACL" << item.hasExtendedACL();
    qDebug() << "ACL" << item.ACL().asString();
    qDebug() << "isValid" << item.ACL().isValid();
    qDebug() << "isExtended" << item.ACL().isExtended();
    // qDebug() << "fileSystemSupportsACL" << fileSystemSupportsACL(path.toUtf8());

    return app.exec();
}
